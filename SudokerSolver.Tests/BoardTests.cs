﻿using SudokerSolver.Core;
using Xunit;

namespace SudokerSolver.Tests
{
    public class BoardTests
    {
        [Fact]
        public void Board_Ctor_Test()
        {
            var board = new Board();
        }

        [Fact]
        public void Board_Simple_GetValue_Test()
        {
            var board = new Board();
            board.AddKnownValue(1, 3, 5);
            board.AddKnownValue(8, 7, 4);
            Assert.Equal(5, board.GetValue(1, 3));
            Assert.Equal(4, board.GetValue(8, 7));
            Assert.Equal(0, board.GetValue(1, 1));
        }

        [Fact]
        public void Board_Simple_GetResult_Test()
        {
            var board = new Board();
            board.AddKnownValue(1, 3, 5);
            board.AddKnownValue(8, 7, 4);
            var actualResult = board.GetResult();
            var expectedResult = new int[9, 9];
            expectedResult[1, 3] = 5;
            expectedResult[8, 7] = 4;
            AssertResult(expectedResult, actualResult);
        }

        [Fact]
        public void Board_Simple_InferSingleMissingRowValue_Test()
        {
            // Create the board with a row entirely filled in except two
            var board = new Board();
            board.AddKnownValue(0, 0, 1);
            board.AddKnownValue(0, 1, 2);
            board.AddKnownValue(0, 2, 3);
            board.AddKnownValue(0, 3, 4);
            board.AddKnownValue(0, 4, 5);
            board.AddKnownValue(0, 5, 6);
            board.AddKnownValue(0, 6, 7);

            // Make sure all values are know except those two
            var expectedResult = new int[9, 9];
            expectedResult[0, 0] = 1;
            expectedResult[0, 1] = 2;
            expectedResult[0, 2] = 3;
            expectedResult[0, 3] = 4;
            expectedResult[0, 4] = 5;
            expectedResult[0, 5] = 6;
            expectedResult[0, 6] = 7;
            expectedResult[0, 7] = 0;
            expectedResult[0, 8] = 0;

            var actualResult = board.GetResult();
            AssertResult(expectedResult, actualResult);

            // Fill in one of the two and make sure the last is inferred
            board.AddKnownValue(0, 7, 8);
            expectedResult[0, 7] = 8;
            expectedResult[0, 8] = 9;

            actualResult = board.GetResult();
            AssertResult(expectedResult, actualResult);
        }

        [Fact]
        public void Board_Simple_InferSingleMissingColumnValue_Test()
        {
            // Create the board with a column entirely filled in except two
            var board = new Board();
            board.AddKnownValue(0, 0, 1);
            board.AddKnownValue(1, 0, 2);
            board.AddKnownValue(2, 0, 3);
            board.AddKnownValue(3, 0, 4);
            board.AddKnownValue(4, 0, 5);
            board.AddKnownValue(5, 0, 6);
            board.AddKnownValue(6, 0, 7);

            // Make sure all values are know except those two
            var expectedResult = new int[9, 9];
            expectedResult[0, 0] = 1;
            expectedResult[1, 0] = 2;
            expectedResult[2, 0] = 3;
            expectedResult[3, 0] = 4;
            expectedResult[4, 0] = 5;
            expectedResult[5, 0] = 6;
            expectedResult[6, 0] = 7;
            expectedResult[7, 0] = 0;
            expectedResult[8, 0] = 0;

            var actualResult = board.GetResult();
            AssertResult(expectedResult, actualResult);

            // Fill in one of the two and make sure the last is inferred
            board.AddKnownValue(7, 0, 8);
            expectedResult[7, 0] = 8;
            expectedResult[8, 0] = 9;

            actualResult = board.GetResult();
            AssertResult(expectedResult, actualResult);
        }

        [Fact]
        public void Board_Simple_InferSingleMissingNonetValue_Test()
        {
            // Create the board with a nonet entirely filled in except two
            var board = new Board();
            board.AddKnownValue(0, 0, 1);
            board.AddKnownValue(0, 1, 2);
            board.AddKnownValue(0, 2, 3);
            board.AddKnownValue(1, 0, 4);
            board.AddKnownValue(1, 1, 5);
            board.AddKnownValue(1, 2, 6);
            board.AddKnownValue(2, 0, 7);

            // Make sure all values are know except those two
            var expectedResult = new int[9, 9];
            expectedResult[0, 0] = 1;
            expectedResult[0, 1] = 2;
            expectedResult[0, 2] = 3;
            expectedResult[1, 0] = 4;
            expectedResult[1, 1] = 5;
            expectedResult[1, 2] = 6;
            expectedResult[2, 0] = 7;
            expectedResult[2, 1] = 0;
            expectedResult[2, 2] = 0;

            var actualResult = board.GetResult();
            AssertResult(expectedResult, actualResult);

            // Fill in one of the two and make sure the last is inferred
            board.AddKnownValue(2, 1, 8);
            expectedResult[2, 1] = 8;
            expectedResult[2, 2] = 9;

            actualResult = board.GetResult();
            AssertResult(expectedResult, actualResult);
        }

        #region Private helpers

        void AssertResult(int[,] expectedResult, int[,] actualResult)
        {
            Assert.Equal(expectedResult.Length, actualResult.Length);
            Assert.Equal(expectedResult.GetLength(0), actualResult.GetLength(0));
            Assert.Equal(expectedResult.GetLength(1), actualResult.GetLength(1));

            for (var r = 0; r < expectedResult.GetLength(0); r++)
            {
                for (var c = 0; c < expectedResult.GetLength(1); c++)
                {
                    Assert.Equal(expectedResult[r, c], actualResult[r, c]);
                }
            }
        }

        #endregion
    }
}
