﻿using SudokerSolver.Core;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using Xunit;

namespace SudokerSolver.Tests
{
    public class SolverTests
    {
        [Theory]
        [InlineData("EasyExample1")]
        [InlineData("MediumExample1", Skip = "Can't pass a medium case right now")]
        [InlineData("HardExample1", Skip = "Can't pass a hard case right now")]
        public void Solver_Comparison_Test(string testCaseName)
        {
            var boardLines = ReadEmbeddedResource($"{testCaseName}-Board.txt").Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            var board = new Board();
            for (var rowIndex = 0; rowIndex < 9; rowIndex++)
            {
                var boardLine = boardLines[rowIndex];
                for (var columnIndex = 0; columnIndex < 9; columnIndex++)
                {
                    var character = boardLine[columnIndex];
                    if (char.IsDigit(character))
                    {
                        board.AddKnownValue(rowIndex, columnIndex, character - '0');
                    }
                }
            }

            var expectedSolutionLines = ReadEmbeddedResource($"{testCaseName}-Solution.txt").Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            var result = board.GetResult();

            var ms = new MemoryStream();
            board.Print(ms, true);
            ms.Position = 0;
            var output = new StreamReader(ms).ReadToEnd();

            for (var rowIndex = 0; rowIndex < 9; rowIndex++)
            {
                var actualSolutionLine = new StringBuilder();
                for (var columnIndex = 0; columnIndex < 9; columnIndex++)
                {
                    var value = result[rowIndex, columnIndex];
                    actualSolutionLine.Append(value > 0 ? value.ToString() : " ");
                }
                Assert.Equal(expectedSolutionLines[rowIndex], actualSolutionLine.ToString());
            }
        }

        #region Private helpers

        string ReadEmbeddedResource(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.EmbeddedFiles.{fileName}"))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        #endregion
    }
}
