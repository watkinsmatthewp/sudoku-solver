﻿using System.Linq;

namespace SudokerSolver.Core
{
    public static class CellValuePossibilitySetExtensions
    {
        public static bool IsKnownValue(this ICellValuePossbilitySet cellValuePossbilitySet)
            => cellValuePossbilitySet.PossibleValueCount == 1;

        public static CellValuePossibilitySetComparisonResult CompareWith(this ICellValuePossbilitySet a, ICellValuePossbilitySet b)
        {
            var result = new CellValuePossibilitySetComparisonResult();

            result.InBoth.AddRange(a.AllPossibleValues.Intersect(b.AllPossibleValues));
            result.OnlyInA.AddRange(a.AllPossibleValues.Except(result.InBoth));
            result.OnlyInB.AddRange(b.AllPossibleValues.Except(result.InBoth));

            return result;
        }
    }
}
