﻿using System;
using System.Collections.Generic;

namespace SudokerSolver.Core
{
    public interface ICellValuePossbilitySet
    {
        int Value { get; }
        int PossibleValueCount { get; }
        IEnumerable<int> AllPossibleValues { get; }
    }
}
