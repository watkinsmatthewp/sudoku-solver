﻿using System.Collections.Generic;

namespace SudokerSolver.Core
{
    public interface IBoardPossibilityGraph
    {
        /// <summary>
        /// Excludes the specified value from the potential values for that cell
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <param name="notValue"></param>
        /// <returns>true if the value was a potential value before the update</returns>
        bool RuleOutPotentialValue(int rowIndex, int columnIndex, int notValue);

        /// <summary>
        /// Gets the list of potential values for the specified cell
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns>the list of potential values</returns>
        ICellValuePossbilitySet GetCell(int rowIndex, int columnIndex);
    }
}
