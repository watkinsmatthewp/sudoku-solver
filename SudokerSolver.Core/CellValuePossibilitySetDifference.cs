﻿using System.Collections.Generic;

namespace SudokerSolver.Core
{
    public class CellValuePossibilitySetComparisonResult
    {
        public List<int> OnlyInA { get; set; } = new List<int>();
        public List<int> OnlyInB { get; set; } = new List<int>();
        public List<int> InBoth { get; set; } = new List<int>();
        public bool AContainsB => OnlyInB.Count == 0;
        public bool BContainsA => OnlyInA.Count == 0;
        public bool AEqualsB => AContainsB && BContainsA;
    }
}
