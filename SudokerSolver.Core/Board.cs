﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SudokerSolver.Core
{
    public class Board
    {
        const int BOARD_SIZE = 9;
        const int NONET_SIZE = 3;

        static readonly int[] ALL_VALUES = Enumerable.Range(1, BOARD_SIZE).ToArray();

        IBoardPossibilityGraph _possibilityGraph;
        Dictionary<Coordinate, int> _knownValues = new Dictionary<Coordinate, int>();

        public Board() : this(new IntegerListBoardPossibilityGraph())
        {

        }

        public Board(IBoardPossibilityGraph possibilityGraph)
        {
            _possibilityGraph = possibilityGraph ?? throw new ArgumentNullException(nameof(possibilityGraph));
        }

        public void AddKnownValue(int rowIndex, int columnIndex, int knownValue)
            => AddKnownValue(new Coordinate(rowIndex, columnIndex), knownValue);

        public void AddKnownValue(Coordinate coordinate, int knownValue)
        {
            foreach (var notValue in ALL_VALUES.Except(knownValue))
            {
                RuleOutValue(coordinate, notValue);
            }
        }

        public int[,] GetResult()
        {
            var result = new int[BOARD_SIZE, BOARD_SIZE];
            for (var rowIndex = 0; rowIndex < BOARD_SIZE; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < BOARD_SIZE; columnIndex++)
                {
                    result[rowIndex, columnIndex] = GetValue(rowIndex, columnIndex);
                }
            }
            return result;
        }

        public int GetValue(int rowIndex, int columnIndex)
            => GetValue(new Coordinate(rowIndex, columnIndex));

        public int GetValue(Coordinate coordinate)
            => _possibilityGraph.GetCell(coordinate.RowIndex, coordinate.ColumnIndex).Value;

        public void Print(Stream destinationStream, bool showPotentialValues)
        {
            var writer = new StreamWriter(destinationStream) { AutoFlush = true };
            for (var boardRowIndex = 0; boardRowIndex < BOARD_SIZE; boardRowIndex++)
            {
                if (showPotentialValues)
                {
                    for (var subRowIndex = 0; subRowIndex < NONET_SIZE; subRowIndex++)
                    {
                        for (var boardColumnIndex = 0; boardColumnIndex < BOARD_SIZE; boardColumnIndex++)
                        {
                            var cell = _possibilityGraph.GetCell(boardRowIndex, boardColumnIndex);
                            for (var subColumnIndex = 0; subColumnIndex < NONET_SIZE; subColumnIndex++)
                            {
                                var val = (NONET_SIZE * subRowIndex) + subColumnIndex + 1;
                                writer.Write(cell.AllPossibleValues.Contains(val) ? val.ToString() : " ");
                            }
                            writer.Write("|");
                        }
                        writer.Write(Environment.NewLine);
                    }
                    writer.WriteLine("-");
                }
                else
                {
                    for (var boardColumnIndex = 0; boardColumnIndex < BOARD_SIZE; boardColumnIndex++)
                    {
                        var cell = _possibilityGraph.GetCell(boardRowIndex, boardColumnIndex);
                        writer.Write(cell.Value > 0 ? cell.Value.ToString() : " ");
                    }
                    writer.Write(Environment.NewLine);
                }
            }
            writer.Flush();
        }

        #region Private helpers

        void RuleOutValue(Coordinate coordinate, int value)
        {
            if (_possibilityGraph.RuleOutPotentialValue(coordinate.RowIndex, coordinate.ColumnIndex, value))
            {
                PropagateExclusion(coordinate, GetAllNonetCoordinates(coordinate));
                PropagateExclusion(coordinate, GetAllRowCoordinates(coordinate));
                PropagateExclusion(coordinate, GetAllColumnCoordinates(coordinate));
            }
        }

        void PropagateExclusion(Coordinate referenceCoordinate, IEnumerable<Coordinate> coordinates)
        {
            var nextRuleOuts = new Dictionary<Coordinate, HashSet<int>>();
            var coordinateGroups = coordinates.GroupBy(p => _possibilityGraph.GetCell(p.RowIndex, p.ColumnIndex)).ToDictionary(g => g.Key, g => g.ToArray());
            var referenceGroup = coordinateGroups.First(g => g.Value.Contains(referenceCoordinate));
            var referenceGroupIsComplete = referenceGroup.Value.Length == referenceGroup.Key.PossibleValueCount;

            foreach (var coordinateGroup in coordinateGroups.Except(referenceGroup))
            {
                var comparison = referenceGroup.Key.CompareWith(coordinateGroup.Key);
                if (comparison.BContainsA && referenceGroupIsComplete)
                {
                    foreach (var exclusionValue in referenceGroup.Key.AllPossibleValues)
                    {
                        foreach (var coordinate in coordinateGroup.Value)
                        {
                            if (!nextRuleOuts.ContainsKey(coordinate))
                            {
                                nextRuleOuts[coordinate] = new HashSet<int>();
                            }
                            nextRuleOuts[coordinate].Add(exclusionValue);
                        }
                    }
                }
                else if (comparison.AContainsB && coordinateGroup.Key.PossibleValueCount == coordinateGroup.Value.Length)
                {
                    foreach (var exclusionValue in coordinateGroup.Key.AllPossibleValues)
                    {
                        foreach (var coordinate in referenceGroup.Value)
                        {
                            if (!nextRuleOuts.ContainsKey(coordinate))
                            {
                                nextRuleOuts[coordinate] = new HashSet<int>();
                            }
                            nextRuleOuts[coordinate].Add(exclusionValue);
                        }
                    }
                }
            }

            foreach (var ruleOut in nextRuleOuts)
            {
                foreach (var value in ruleOut.Value)
                {
                    RuleOutValue(ruleOut.Key, value);
                }
            }
        }

        IEnumerable<Coordinate> GetUnknownPeerCoordinates(Coordinate coordinate)
            => GetAllPeerCoordinates(coordinate).Except(_knownValues.Keys);

        static IEnumerable<Coordinate> GetAllPeerCoordinates(Coordinate coordinate)
            => GetPeerNonetCoordinates(coordinate)
            .Concat(GetPeerRowCoordinates(coordinate))
            .Concat(GetPeerColumnCoordinates(coordinate));

        static IEnumerable<Coordinate> GetPeerRowCoordinates(Coordinate coordinate)
            => GetAllRowCoordinates(coordinate).Where(c => c.ColumnIndex != coordinate.ColumnIndex);

        static IEnumerable<Coordinate> GetPeerColumnCoordinates(Coordinate coordinate)
            => GetAllColumnCoordinates(coordinate).Where(c => c.RowIndex != coordinate.RowIndex);

        static IEnumerable<Coordinate> GetPeerNonetCoordinates(Coordinate memberCellCoordinate)
            => GetAllNonetCoordinates(memberCellCoordinate).Where(c => c.RowIndex != memberCellCoordinate.RowIndex && c.ColumnIndex != memberCellCoordinate.ColumnIndex);

        static IEnumerable<Coordinate> GetAllRowCoordinates(Coordinate memberCellCoordinate)
            => GetAllRowCoordinates(memberCellCoordinate.RowIndex);

        static IEnumerable<Coordinate> GetAllRowCoordinates(int rowIndex)
        {
            for (var columnIndex = 0; columnIndex < BOARD_SIZE; columnIndex++)
            {
                yield return new Coordinate(rowIndex, columnIndex);
            }
        }

        static IEnumerable<Coordinate> GetAllColumnCoordinates(Coordinate memberCellCoordinate)
            => GetAllColumnCoordinates(memberCellCoordinate.ColumnIndex);

        static IEnumerable<Coordinate> GetAllColumnCoordinates(int columnIndex)
        {
            for (var rowIndex = 0; rowIndex < BOARD_SIZE; rowIndex++)
            {
                yield return new Coordinate(rowIndex, columnIndex);
            }
        }

        static IEnumerable<Coordinate> GetAllNonetCoordinates(Coordinate memberCellCoordinate)
            => GetAllNonetCoordinates(memberCellCoordinate.RowIndex / NONET_SIZE, memberCellCoordinate.ColumnIndex / NONET_SIZE);

        static IEnumerable<Coordinate> GetAllNonetCoordinates(int nonetRowIndex, int nonetColumnIndex)
        {
            var initialRowIndex = nonetRowIndex * NONET_SIZE;
            var initialColumnIndex = nonetColumnIndex * NONET_SIZE;
            for (var rowOffset = 0; rowOffset < NONET_SIZE; rowOffset++)
            {
                for (var columnOffset = 0; columnOffset < NONET_SIZE; columnOffset++)
                {
                    yield return new Coordinate(initialRowIndex + rowOffset, initialColumnIndex + columnOffset);
                }
            }
        }

        #endregion
    }
}
