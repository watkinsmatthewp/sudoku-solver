﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SudokerSolver.Core
{
    public class IntegerListCellValuePossibilitySet : ICellValuePossbilitySet
    {
        readonly int _boardSize;
        readonly HashSet<int> _possibleValues;

        public int Value => _possibleValues.SafeSingleOrDefault();
        public int PossibleValueCount => _possibleValues.Count;
        public IEnumerable<int> AllPossibleValues => _possibleValues;

        public IntegerListCellValuePossibilitySet(int boardSize, HashSet<int> possibleValues)
        {
            _boardSize = boardSize;
            _possibleValues = possibleValues ?? throw new ArgumentNullException(nameof(possibleValues));
        }

        public override int GetHashCode()
        {
            var hashCode = 0;
            for (var i = 0; i < _boardSize; i++)
            {
                hashCode += _possibleValues.Contains(i + 1) ? 1 : 0;
                hashCode <<= 1;
            }
            return hashCode;
        }

        public override bool Equals(object obj) => GetHashCode() == obj.GetHashCode();
    }
}
