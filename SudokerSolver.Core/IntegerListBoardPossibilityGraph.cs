﻿using System.Collections.Generic;
using System.Linq;

namespace SudokerSolver.Core
{
    public class IntegerListBoardPossibilityGraph : IBoardPossibilityGraph
    {
        const int BOARD_SIZE = 9;
        static readonly int[] ALL_VALUES = Enumerable.Range(1, BOARD_SIZE).ToArray();

        HashSet<int>[,] _board = new HashSet<int>[BOARD_SIZE, BOARD_SIZE];

        public IntegerListBoardPossibilityGraph()
        {
            for (var rowIndex = 0; rowIndex < BOARD_SIZE; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < BOARD_SIZE; columnIndex++)
                {
                    _board[rowIndex, columnIndex] = new HashSet<int>(ALL_VALUES);
                }
            }
        }

        public bool RuleOutPotentialValue(int rowIndex, int columnIndex, int notValue)
            => _board[rowIndex, columnIndex].Remove(notValue);

        public ICellValuePossbilitySet GetCell(int rowIndex, int columnIndex)
            => new IntegerListCellValuePossibilitySet(BOARD_SIZE, _board[rowIndex, columnIndex]);
    }
}
