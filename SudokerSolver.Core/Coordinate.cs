﻿namespace SudokerSolver.Core
{
    public struct Coordinate
    {
        public int RowIndex;
        public int ColumnIndex;

        public Coordinate(int rowIndex, int columnIndex)
        {
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
        }
    }
}
