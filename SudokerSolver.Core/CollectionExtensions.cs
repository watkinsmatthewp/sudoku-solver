﻿using System.Collections.Generic;
using System.Linq;

namespace SudokerSolver.Core
{
    static class CollectionExtensions
    {
        internal static IEnumerable<T> Except<T>(this IEnumerable<T> collection, T val) => collection.Except(new T[] { val });

        internal static T SafeSingleOrDefault<T>(this IEnumerable<T> collection)
        {
            var valueFound = false;
            var value = default(T);
            foreach (var item in collection)
            {
                if (valueFound)
                {
                    return default(T);
                }

                value = item;
                valueFound = true;
            }
            return value;
        }
    }
}
