﻿using System;
using System.Collections.Generic;

namespace SudokerSolver.Core
{
    public class BoolArrayBoardPossibilityGraph : IBoardPossibilityGraph
    {
        const int BOARD_SIZE = 9;

        static readonly int CELL_FLAGS_COUNT = BOARD_SIZE;
        static readonly int GROUP_FLAGS_COUNT = (int)Math.Pow(BOARD_SIZE, 2);
        static readonly int BOARD_FLAGS_COUNT = (int)Math.Pow(BOARD_SIZE, 3);

        bool[] _flags = new bool[BOARD_FLAGS_COUNT];

        public BoolArrayBoardPossibilityGraph()
        {
            for (var i = 0; i < _flags.Length; i++)
            {
                _flags[i] = true;
            }
        }

        public bool RuleOutPotentialValue(int rowIndex, int columnIndex, int notValue)
            => _flags[GetValueFlagIndex(rowIndex, columnIndex, notValue)] = false;

        public ICellValuePossbilitySet GetCell(int rowIndex, int columnIndex)
        {
            var cellFlagIndex = GetCellFlagIndex(rowIndex, columnIndex);
            return new BoolArrayCellValuePossibilitySet(_flags, cellFlagIndex, CELL_FLAGS_COUNT);
        }

        #region Private helpers

        static int GetValueFlagIndex(int rowIndex, int columnIndex, int value) => GetCellFlagIndex(rowIndex, columnIndex) + GetValueFlagOffset(value);
        static int GetValueFlagOffset(int value) => value - 1;
        static int GetCellFlagIndex(int rowIndex, int columnIndex) => GetRowFlagIndex(rowIndex) + (columnIndex * CELL_FLAGS_COUNT);
        static int GetRowFlagIndex(int rowIndex) => rowIndex * GROUP_FLAGS_COUNT;

        #endregion
    }
}
