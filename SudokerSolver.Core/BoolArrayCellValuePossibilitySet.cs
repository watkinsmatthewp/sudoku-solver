﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SudokerSolver.Core
{
    public class BoolArrayCellValuePossibilitySet : ICellValuePossbilitySet
    {
        readonly bool[] _flags;
        readonly int _cellFlagIndex;
        readonly int _cellFlagsCount;

        public int Value => AllPossibleValues.SafeSingleOrDefault();
        public int PossibleValueCount => AllPossibleValues.Count();

        public IEnumerable<int> AllPossibleValues
        {
            get
            {
                for (var i = 0; i < _cellFlagsCount; i++)
                {
                    if (_flags[_cellFlagIndex + i])
                    {
                        yield return i + 1;
                    }
                }
            }
        }

        public BoolArrayCellValuePossibilitySet(bool[] flags, int cellFlagIndex, int cellFlagsCount)
        {
            _flags = flags ?? throw new ArgumentNullException(nameof(flags));
            _cellFlagIndex = cellFlagIndex;
            _cellFlagsCount = cellFlagsCount;
        }

        public override int GetHashCode()
        {
            var hashCode = 0;
            for (var i = 0; i < _cellFlagsCount; i++)
            {
                hashCode += _flags[_cellFlagIndex + i] ? 1 : 0;
                hashCode <<= 1;
            }
            return hashCode;
        }

        public override bool Equals(object obj) => GetHashCode() == obj.GetHashCode();
    }
}
